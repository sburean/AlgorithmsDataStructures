package code;

public class MyQuickSort {

	//https://www.youtube.com/watch?v=SLauY6PpjW4 -> stack overflow error
	
	public static <T extends Comparable<T>> void quickSort(T[] array, int left, int right){
		
		if(left >= right) return; //this array is already sorted
		
		T pivot = array[(right-left)/2]; 
		int index = partition(array, left, right, pivot);
		quickSort(array, left, index-1); //sort left side of array until index -1 (so we're not overlapping)
		quickSort(array, index, right);
		
	}
	
	private static <T extends Comparable<T>> int partition(T[] array, int left, int right, T pivot){
		
		while(left <= right){
			
			
			while(array[left].compareTo(pivot) < 0){
				left++;
			}
			
			while(array[right].compareTo(pivot) > 0){
				right--;
			}
			
			//make sure we didn't iterate PAST each pointer in the above whiles:
			if(array[left].compareTo(array[right]) > 0){
				//items here have to be swapped, then increment BOTH indices 
				swap(array, left, right); 
				left++;
				right--;
			}
			
		}
		
		return left; //partition border = left index
		
	}
	
	private static <T extends Comparable<T>> void swap(T[] array, int left, int right){
		
		T tmpPtr = array[right];
		array[right] = array[left];
		array[left] = tmpPtr;
		
	}
	
	public static <T> void print(T[] array, int start, int end){
		
		int lastIndex = end - 1; 
		System.out.print("[");
		for(int i = start; i < end; i++){
			if(i != lastIndex) System.out.print(array[i]+"][");
			else System.out.print(array[i]);
		}
		System.out.println("]");
		
	}
	
}

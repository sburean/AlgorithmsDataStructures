package singleLinked;

public class TestStub {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyLinkedList<Integer> ll = new MyLinkedList<>();
		
		ll.add(5);
		ll.add(4);
		ll.add(2);
		ll.add(12);
		
		
		ll.display();
		System.out.println("List size = " + ll.getSize());
		if(ll.exists(4)) System.out.println("4 exists in the list"); else System.out.println("4 does NOT exist in the list");
		if(ll.exists(11)) System.out.println("11 exists in the list"); else System.out.println("11 does NOT exist in the list");
		System.out.println("===============================================");
		
		System.out.println("deleting 2:");
		ll.delete(2);
		ll.display();
		System.out.println("List size = " + ll.getSize());
		System.out.println("===============================================");
		
		System.out.println("deleting 4:");
		ll.delete(4);		
		ll.display();
		System.out.println("List size = " + ll.getSize());
		System.out.println("===============================================");
		
		if(ll.exists(4)) System.out.println("4 exists in the list"); else System.out.println("4 does NOT exist in the list");
		if(ll.exists(11)) System.out.println("11 exists in the list"); else System.out.println("11 does NOT exist in the list");
		
	}

}

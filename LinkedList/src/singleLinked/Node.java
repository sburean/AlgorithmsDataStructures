package singleLinked;

/**
 * Node in the singly linked list. Contains data and reference to next node. 
 * @author Szabi Burean
 *
 */
public class Node<T> {
	
	private T data;
	public Node<T> next;
	
	public Node(T data) {
		this.data = data;
	}
	
	public T getData(){
		return this.data;
	}
	
	public void setData(T data){
		this.data = data;
	}
	
	@Override
	public String toString() {
		return data.toString();
	}

}//Node<T>

package ex1;

public class SolvePostFix {

	/*
	 * Algorithm for solving an expression in the postfix notation is simple.
	 * Simply loop through (left-to-right) postfix expression, and if
	 * - operand, push onto stack
	 * - operator, pop stack twoce to get two most recent operands, and perform computation
	 * once have result of computation, pop it back onto stack
	 * keep scanning until reach end of expression
	 * at which point, if we still have anything on the stack, pop them and perform computation with last operator. 
	 */
	
	//This works because of postfix notation property:
	//1: first two elements in expression are always operands
	//2: last element in expression is always an operator
	
}

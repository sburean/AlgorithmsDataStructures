package singleLinked;

/**
 * Single linked list implementation with add, delete, display, and search
 * @author Szabi Burean
 *
 */
public class MyLinkedList<T> {
	
	public Node<T> mHead;
	private int mSize;
	
	public MyLinkedList(){
		//explicitly initialize state to default, but don't need to as instance variables are automatically initialized to defaults:
		this.mHead = null;
		mSize = 0;
	}
	
	public int getSize(){
		return this.mSize;
	}
	
	/**
	 * Adds specified data as a new node to the start of the linked list.
	 * @param data to add to the list
	 */
	public void add(T data){
		
		//NOTE: Can add to end of list, but then we'd have to iterate over list each time; much more inefficient. 
		//		Could keep a tail reference to make sure we don't have to do that.
		
		//Create new node
		Node<T> newNode = new Node<>(data);
		
		//set new node's next reference to the current head
		newNode.next = mHead;
		
		//set head reference to new node
		this.mHead = newNode;
		
		//increment size
		incSize();
		
	}//add
	
	/**
	 * Checks to see if a node with the specified data exists in the list
	 * @param data that may or may not exist in list within a node
	 * @return TRUE if a node contains the data in the list, FALSE otherwise
	 */
	public boolean exists(T data){
		Node<T> current = mHead;
		
		while(current != null){
			
			//For custom types, assume it provides a valid .equals(...) method
			if(current.getData().equals(data)) return true;
			else current = current.next;
			
		}
		
		return false;
		
	}//exists
	
	/**
	 * Deletes a node from the linked list containing the specified data (if it exists)
	 * @param data that should be removed from the list
	 * @return TRUE if a node was successfully deleted, FALSE otherwise
	 */
	public boolean delete(T data){
		Node<T> current = mHead;
		
		while(current != null){
			
			//Peek ahead & see if it's what we want to delete	
			if( (current.next.getData().equals(data)) && (current.next != null) ){
				//Found node we want to delete as the next node, and it's not the tail node
								
				//Get a reference to the next node after the one we want to delete.
				Node<T> toSetAsCurrentNext = current.next.next;
				
				//"Delete" the next node (our target)
				current.next = null;
				
				//Now update current's next reference to the (now) deleted node's next 
				current.next = toSetAsCurrentNext;
				
				decSize();
				return true;
				
			} else if( (current.next.getData().equals(data)) && (current.next == null) ){
				//Found node we want to delete as the next node, and it's the tail node
				
				//simply set current's next to null as the new tail
				current.next = null;
				
				decSize();
				return true;
				
			} else {	
				current = current.next;
			}
			
		}//while
		
		System.out.println("ERR: Data is not in list!");
		return false;
		
	}//delete
	
	/**
	 * Displays the linked list:
	 */
	public void display(){
		
		System.out.print("Head->");
		
		Node<T> current = mHead;
		while(current != null){ //while we have a next node
			System.out.print(current.getData()+"->");
			current = current.next;
		}
		
		System.out.print("Null\n");
		
	}
	
	private void incSize(){
		mSize++;
	}
	
	private void decSize(){
		mSize--;
	}

}

package doublyLinked;

public class MyDoublyLinkedList<T> {

	/*
	 * NOTE: only providing more complex operations right now, can add simpler ones later.
	 * List of operations:
	 * - addToStart
	 * - delete(T data)
	 * - addToEnd
	 * - deleteFromEnd
	 * - addAfter(T data)
	 * - display
	 * - displayBefore(T data)
	 * - displayAfter(T data)
	 * - deleteAll()
	 */
	
	public Node<T> mHead;
	private int mSize;
	
	public MyDoublyLinkedList() {}
	
	public int getSize(){
		return this.mSize;
	}
	
	private void incSize(){
		mSize++;
	}
	
	private void decSize(){
		mSize--;
	}
	
	/**
	 * Checks to see if the linked list has any elements in it.
	 * @return TRUE if the list is empty, FALSE otherwise
	 */
	public boolean isEmpty(){
		return mHead == null;
	}
	
	/**
	 * Adds specified data to the start of the doubly linked list.
	 * @param data to add to the list
	 */
	public void addToStart(T data){
		
		Node<T> nodeToAdd = new Node<>(data);
		
		//Update next/prev references for the (new) head node
		nodeToAdd.next = mHead;
		nodeToAdd.prev = null;
		
		if(!isEmpty()){
			//list is not empty, therefore head refers to valid node:
			
			//update head's previous reference to the new node
			mHead.prev = nodeToAdd;
		}
		
		//update the head reference:
		mHead = nodeToAdd;
		
		//increment size
		incSize();
		
	}//addToStart
	
	
	/**
	 * Adds data to the list after the specified data. If data to add after doesn't exist, 
	 * will add data to the start of the list.
	 * @param dataToAdd data that is being added to the list
	 * @param afterData data after which the new data should be added
	 */
	public void addAfter(T dataToAdd, T afterData){
		
		Node<T> current = mHead;
		while(current != null){
			
			if(current.getData().equals(afterData)){
				
				if(current.next == null){
					//we're adding after the tail node, ie: adding to the last position in the list
					
					Node<T> nodeToAdd = new Node<>(dataToAdd);
					
					//set references
					nodeToAdd.next = null;
					nodeToAdd.prev = current;
					
					//update references
					current.next = nodeToAdd;
					
					incSize();
					return;
					
				} else {
					//Node we're adding after is not the last node
					
					Node<T> nodeToAdd = new Node<>(dataToAdd);
					
					//set references:
					nodeToAdd.next = current.next;
					nodeToAdd.prev = current;
					
					//update list's link references
					current.next = nodeToAdd;
					current.next.prev = nodeToAdd;
					
					incSize();
					return;
				}//if-else adding to tail
				
			}//if we found node we're appending after
			
			current = current.next;
			
		}//while
		
		//node to add after doesn't exist, append new data to the start of the list
		addToStart(dataToAdd);
		
	}//addAdter
	
	/**
	 * Deletes node form the doubly linked list that contains the specified data.
	 * @param data to be removed from the doubly linked list 
	 */
	public boolean delete(T data){
		
		Node<T> current = mHead;
		while(current != null){ //search for node to delete
			
			if(current.getData().equals(data)){
				//found item we're deleting
				
				if(current == mHead){
					//data to delete is at the HEAD of the list
					
					/*
					 * This will also trigger if we only have one element in the list, before next else if(..) below.
					 */
					
					if(getSize() == 1){
						//only one item in the list
						mHead = null;
						
					} else {
						//Update references:
						mHead = current.next;
						current.next.prev = null;						
					}
										
					current = null;
					decSize();
					
					return true;
					
				} else if(current.next == null){
					//data to delete is at the TAIL of the list
					
					//update references:
					current.prev.next = null;
					
					current = null;
					decSize();
					
					return true;
					
				} else {
					//data to delete is in the middle of the list
					
					//left  node's next = current's next
					current.prev.next = current.next;
					
					//right node's previous = current's prev
					current.next.prev = current.prev;
					
					current = null;
					decSize();		
					
					return true;
				}
				
			} else {
				current = current.next;
			}
			
		}//while
		
		System.out.println("ERR: Data is not in list!");
		return false;
		
	}//delete
	
	
	
	/**
	 * Prints the doubly linked list
	 */
	public void display(){
		
		System.out.print("Head->");
		
		Node<T> current = mHead;
		while(current != null){
			
			if(current.next == null){
				//next node is null, only print single arrow
				System.out.print("["+current.getData()+"]->");
			} else {
				System.out.print("["+current.getData()+"]=");				
			}

			current = current.next;
		}
		
		System.out.print("Null\n");
		
		
	}//display
	
	
}//MyDoublyLinkedList

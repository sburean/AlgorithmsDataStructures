package code;

public class MyMain {
	
	public static void main(String[] args) {
		
		int[] array1 = {0,1,2,3,4,5,6,7,8,9}; //declaring + initializing, array of size 10
		int[] array2 = new int[10]; //declaring only, array of size 10.
		
		insertAlgo();
		System.out.println();
		deleteAlgo();
		
		/**
		 * NOTE: 
		 * All of these operations on an array are implemented through the ArrayList<T> collection!
		 * 
		 * For example, see that in the DELETE operation, we would have to re-size the array after each deletion. 
		 * However, using an ArrayList implementation we don't have to. Only way to get ArrayList's size is through 
		 * the size() method if underlying array is properly encapsulated. Therefore, the Arraylist can simply keep
		 * the array's size in an instance variable, update it as needed when deleting, and return that within size(). 
		 */

	}//main
	
	public static void insertAlgo(){
		
		//To make it more efficient, like implementing an ArrayList, can double new size each time to reduce the number of times 
		// the size has to be increased, and effectively reducing the number of times the array values have to be copied over.
		
		//insert:
		int[] array = {5,3,7,9,1}; //5 elements
		int item = 10, k = 4, n = array.length;
		
		
		//print
		System.out.print("before: [");
		for(int p = 0; p < array.length; p++){
			if( p != array.length - 1){
				System.out.print(array[p] + ",");
			} else {
				System.out.print(array[p]);
			}
		}
		System.out.print("]\n");
		
		/* broke up for-loop so we don't have an if i == k check in every iteration */
		int[] newArray = new int[n+1];
		for(int i = 0; i < k; i++){
			newArray[i] = array[i];
		}
		newArray[k] = item;
		for(int j = k; j<n; j++){
			newArray[j+1] = array[j]; 
		}
		
		array = newArray;
		newArray = null;
		
		//print
		System.out.print("after: [");
		for(int p = 0; p < array.length; p++){
			if( p != array.length - 1){
				System.out.print(array[p] + ",");
			} else {
				System.out.print(array[p]);
			}
		}
		System.out.print("]\n");
	}//insertAlgo
	
	public static void deleteAlgo(){
		
		//delete:
		int[] array = {5,3,7,9,1}; //5 elements
		int k = 2, n = array.length;
		
		//print
		System.out.print("before: [");
		for(int p = 0; p < n; p++){
			if( p != n - 1){
				System.out.print(array[p] + ",");
			} else {
				System.out.print(array[p]);
			}
		}
		System.out.print("]\n");
		
		for(int i = k; i < n - 1; i++){
			array[i] = array[i+1];
		}
		
		/* 
		 * Don't create new array, have an unneeded entry in current array. (@last position) 
		 * To remove it, would again have to re-size array, or separately keep track of new index as n--;
		 */
		
		//print
		System.out.print("before: [");
		for(int p = 0; p < n; p++){
			if( p != n - 1){
				System.out.print(array[p] + ",");
			} else {
				System.out.print(array[p]);
			}
		}
		System.out.print("]\n");
		
	}//deleteAlgo

}

package ex1;

import java.util.Stack;

/*
 * Algorithm: ** CURRENTLY DOESN'T ACCOUNT FOR ^, simply have to implement extra token check + logic **
 * 
 * basically this: https://en.m.wikipedia.org/wiki/Shunting-yard_algorithm
 * 
 * START
 * - scan expression from left to right
 * - for each token
 * 		- if operand, append to postFix expression
 * 		- if operator, 
 * 			-for each element in the stack BEFORE an opening bracket '('
 * 				- if it has higher precedence
 * 					-pop that operator from stack and append to postFix expression
 * 				- if it has equal precedence
 * 					-if associativity is left-to-right ( yes for +, -, *, / )
 * 						- pop that operator from stack and append to postFix expression (leftmost operator therefore higher priority if same precedence)
 * 					-if associativity is right-to-left ( only for ^ )
 * 						- do nothing, will get pushed in next line (* the current operand has to be executed BEFORE whatever is on the stack; so push onto stack; LIFO *)
 * 			- push operator onto stack
 * 		- if opening bracket: '('
 * 			- push it onto the stack; represents that the next operators take precedence over anything previously in stack
 * 		- if closing bracket: ')'
 * 			- for each operator in the stack until the opening bracket is found '('
 * 				- pop the operator from the stack and append it to the postFix expression 
 * 			- pop off '(' from the stack
 * - for each operator left in the stack
 * 		- pop the operator and append it to the postFix expression
 * END
 * 
 * @author Szabi
 *
 */
public class InFixToPostFix {

	/**
	 * Converts an INFIX expression into POSTFIX format (reverse polish notation) <br>
	 * NOTE: Assumes input is correctly formated with matching brackets.
	 * @param expression to convert, without spaces
	 * @return String representation of expression in POSTFIX format
	 */
	public static String convert(String expression){
		//would have to REGEX here to make sure only valid characters are in expression
		
		
		//ex: A*(B+C) ---> ABC+*
		
		StringBuilder postfix = new StringBuilder();
		Stack<Character> stack = new Stack<>();
		Character onStack = null;
		Character operationToAppend = null;
		
		for(int i = 0; i < expression.length(); i++){
		
			Character token = expression.charAt(i);
			
			if( (token.equals('+')) || (token.equals('-')) ){
				// + or -; two operators with lowest precedence, left-to-right associativity
				
				if( stack.isEmpty() || stack.peek().equals('(') ){
					//stack is empty, nothing to check for, just push token to stack
					stack.push(token);
					continue;
				}
				
				//if stack contains anything of lower precedence, just push token onto stack 
				
				/*
				 * However, nothing lower than + or - ... so everything on stack is higher precedence OR
				 * equal precedence. Therefore we can just pop everything off the stack since we can now
				 * append them to the postFix notation
				 */
				
//				operationToAppend = stack.pop();
//				while( operationToAppend != null ){
//					postfix.append(operationToAppend);
//					if(stack.isEmpty()){
//						operationToAppend = null;
//					} else {
//						if(!stack.peek().equals('(')) operationToAppend = stack.pop();
//						else break;
//					}
//				}
				
				onStack = stack.peek();
				//while top of stack contains operators (all will be equal or higher preceence) and not '(' or empty, pop them off + append
				while( !onStack.equals('(') && !stack.isEmpty()){
					postfix.append(stack.pop());
					onStack = stack.peek();
				}
						
				//and then push the current operation onto the stack
				stack.push(token);
				
			} else if ( (token.equals('*')) || (token.equals('/')) ) {
				// * or /; operators with second highest precedence, left-to-right associativity
				
				if(( stack.isEmpty() || stack.peek().equals('(') )){
					//stack is empty, nothing to check for, just push token to stack
					stack.push(token);
					continue;
				}
			
				onStack = stack.peek();
				//while stack contains operators of HIGHER or EQUAL precedence ( and not empty or '(' ), pop them off + append
				while( (onStack.equals('*') || onStack.equals('/') || onStack.equals('^')) && !onStack.equals('(') && !stack.isEmpty() ){
					postfix.append(stack.pop());
					onStack = stack.peek();
				}
				
//				if ( !(onStack.equals('+') || onStack.equals('-')) ) {
//					//top of stack contains operator of same or higher precedence, pop everything off
//					//ie: NOT anything of lower precedence
//					
//					operationToAppend = stack.pop();
//					while( operationToAppend != null ){
//						postfix.append(operationToAppend);
//						if(stack.isEmpty()){
//							operationToAppend = null;
//						} else {
//							if(!stack.peek().equals('(')) operationToAppend = stack.pop();
//							else break;
//						}
//					}
//				}
				
				//push the current operation onto the stack if top of stack has 
				// operand with lower precedence OR it's a bracket
				stack.push(token);
				
			} else if (token.equals('^')) {
				//highest precedence, right-to-left associativity
				
				/*
				 * Regardless what top of stack contains, always push ^ onto stack.
				 * - If stack is empty or top has '(', push
				 * - If top has LOWER precedence, push
				 * * If top has EQUAL precedence (another ^), still push because association is right-to-left
				 * 		therefore, THIS operator has to execute first, and stacks are LIFO 
				 */
				
				stack.push(token);
				
			} else if (token.equals('(')) {
				//opening bracket, push onto stack
				stack.push(token);
				
			} else if (token.equals(')')) {
				//closing bracket, pop off all operators + append to postfix 
				//	until we reach the opening bracket & pop that off too
				
				operationToAppend = stack.pop();
				while( !operationToAppend.equals('(') ){
					postfix.append(operationToAppend);
					if(!stack.peek().equals('(')) operationToAppend = stack.pop();
					else stack.pop(); break; //pop off '(' and break
				}

			} else {
				//an operand
				postfix.append(token);
			}
			
		}//loop through each character in the expression
		
		//once done looping through, pop each remaining operator off the stack and append to expression
		operationToAppend = stack.pop();
		while( operationToAppend != null){
			postfix.append(operationToAppend);
			operationToAppend = stack.isEmpty() ? null : stack.pop(); 
		}
		
		return postfix.toString();
		
	}//convert
	
}

package code;

public class TestStub {

	public static void main(String[] args) {
		
		Integer[] array = {15,3,2,1,9,5,7,8,6};
		
		MyQuickSort.quickSort(array, 0, array.length-1);
		
		System.out.println("\nSorted:");
		MyQuickSort.print(array, 0, array.length);

	}

}

package asArray;

public class TestStub {

	public static void main(String[] args) {
		
		System.out.println("Created stack:");
		MyStack<Integer> s = new MyStack<>(Integer.class);
		
		s.display();
		System.out.println("Peeking at top: " + s.peek());
		System.out.println("Popping stack...");
		Integer retrieved = s.pop();
		System.out.println("Popped off: " + retrieved);
		
		System.out.println("================================================================================");
		
		System.out.println("pushing 1 and 4 (in that order): ");
		s.push(1);
		s.push(4);
		
		s.display();
		
		System.out.println("================================================================================");
		
		System.out.println("Peeking at top: " + s.peek());
		System.out.println("Popping stack...");
		retrieved = s.pop();
		System.out.println("Popped off: " + retrieved);
		s.display();
		
		System.out.println("Peeking at top: " + s.peek());
		System.out.println("Popping stack...");
		retrieved = s.pop();
		System.out.println("Popped off: " + retrieved);
		s.display();
		
		System.out.println("Popping stack...");
		retrieved = s.pop();
		System.out.println("Popped off: " + retrieved);
		
	}

}

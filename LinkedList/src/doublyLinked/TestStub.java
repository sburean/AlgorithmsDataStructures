package doublyLinked;

public class TestStub {

	public static void main(String[] args) {
		
		System.out.println("creating list..");
		MyDoublyLinkedList<String> dll = new MyDoublyLinkedList<>();
		
		System.out.print("is list empty? ");
		if(dll.isEmpty()) System.out.println("yes"); else System.out.println("no");
		dll.display();
		System.out.println("size: " + dll.getSize());
		
		System.out.println("=====================================================");
		
		System.out.println("adding data:");
		dll.addToStart("toStart");
		dll.addToStart("againToStart");
		dll.addAfter("after\"toStart\"", "toStart");
		
		dll.display();
		System.out.println("size: " + dll.getSize());
		
		System.out.print("is list empty? ");
		if(dll.isEmpty()) System.out.println("yes"); else System.out.println("no");
		
		System.out.println("=====================================================");
		
		System.out.println("deleting data");
		System.out.println("delete: \"start\"");
		dll.delete("start");
		dll.display();
		System.out.println("size: " + dll.getSize());

		System.out.println("----------------------");
		
		System.out.println("delete: \"toStart\"");
		dll.delete("toStart");
		dll.display();
		System.out.println("size: " + dll.getSize());
		
		System.out.println("----------------------");
		
		System.out.println("delete: \"againToStart\"");
		dll.delete("againToStart");
		dll.display();
		System.out.println("size: " + dll.getSize());
		
		System.out.println("----------------------");
		
		System.out.println("=====================================================");
		
		System.out.println("add after \"something\"");
		dll.addAfter("shouldBeFirst", "something");
		dll.display();
		System.out.println("size: " + dll.getSize());
		
		System.out.println("----------------------");
		
		System.out.println("delete item \"shouldBeFirst\": ");
		dll.delete("shouldBeFirst");
		dll.display();
		System.out.println("size: " + dll.getSize());
		System.out.print("is list empty? ");
		if(dll.isEmpty()) System.out.println("yes"); else System.out.println("no");
		
		System.out.println("----------------------");
		
		System.out.println("delete item \" after\"toStart\" \": ");
		dll.delete("after\"toStart\"");
		dll.display();
		System.out.println("size: " + dll.getSize());
		System.out.print("is list empty? ");
		if(dll.isEmpty()) System.out.println("yes"); else System.out.println("no");
		
	}

}

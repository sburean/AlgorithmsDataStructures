package asArray;

import java.lang.reflect.Array;

/**
 * Stack implementation with an array. <br>
 * Supported operations: <br>
 * - pop
 * - push
 */
public class MyStack<T> {

	private final int mStackSize = 5;
	private int mTop = -1;
	private T[] mArray;
	
	@SuppressWarnings("unchecked")
	public MyStack(Class<T> clazz) {
		//create new generic array of constant size 5
		mArray = (T[]) Array.newInstance(clazz, mStackSize);
	}
	
	//-------------Operations----------
	
	/**
	 * Removes and returns data at the top of the stack
	 * @return data that was at the top of the stack, or null if empty.
	 */
	public T pop(){
		
		T dataToReturn = null;
		if(!isEmpty()){
			//if stack is not empty
			
			dataToReturn = mArray[mTop]; //note that data at top is not actually deleted, but top index is decremented.
			mTop--;
			
		} else {
			System.out.println("ERR: Stack is empty!");
		}
		
		return dataToReturn;
		
	}//pop
	
	/**
	 * Pushes new data onto the stack
	 * @param data to place into stack
	 * @return TRUE if successfully pushed, FALSE otherwise
	 */
	public boolean push(T data){
		
		boolean success = false;
		if(!isFull()){
			//if stack is not full
			
			mTop++;
			mArray[mTop] = data;
			success = true;
			
		} else {
			System.out.println("ERR: stack is full!");
		}
		
		return success;
	
	}//push
	
	//-------------Utilities-----------
	
	/**
	 * Check whether the stack is empty
	 * @return TRUE if stack is empty, FALSE otherwise
	 */
	public boolean isEmpty(){
		return mTop < 0;
	}
	
	/**
	 * Check whether the stack is full
	 * @return TRUE if stack is full, FALSE otherwise
	 */
	public boolean isFull(){
		return mTop == (mStackSize - 1); //-1 because last index = one less than size
	}
	
	/**
	 * Returns the top item of the stack without removing it
	 * @return top data element in the stack
	 */
	public T peek(){
		if(!isEmpty()) return mArray[mTop];
		else System.out.println("ERR: Stack is empty!"); return null;
	}
	
	public void display(){
		
		if(isEmpty()){
			System.out.println("ERR: Nothing to display, Stack is empty!");
			return;
		}
		
		for(int i = mTop; i >= 0; i--){
			System.out.println("["+mArray[i].toString()+"]");
		}
		System.out.println("END");
		
	}
	
}//MyStack<T>

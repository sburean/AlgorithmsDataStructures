package ex1;

public class TestStub {

	public static void main(String[] args) {
		
		String expression = "((A+B)*C-D)*E";
		expression = "3+4*2/(1-5)^2^3"; //expect: 342*15-23^^/+
		
		String postFix = InFixToPostFix.convert(expression);
		
		System.out.println(postFix);

	}

}
